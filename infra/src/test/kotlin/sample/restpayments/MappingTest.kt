package sample.restpayments

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import sample.restpayments.domain.NewPayment
import sample.restpayments.domain.PaymentCancellation

class JacksonMappingTest {

    @Before
    fun setup() {

    }

    @Test
    fun testCancellationMapping() {
        val json = """{"reason":"tröt"}"""
        val cancellation: PaymentCancellation = jacksonObjectMapper()
                .reader()
                .forType(PaymentCancellation::class.java)
                .readValue(json)

        assertNotNull(cancellation)

    }

    @Test
    fun testNewPaymentMapping() {
        val json = "{\"amount\":\"123123\"}"
        val newPayment: NewPayment = jacksonObjectMapper()
                .reader()
                .forType(NewPayment::class.java)
                .readValue(json)

        assertNotNull(newPayment)

    }
}