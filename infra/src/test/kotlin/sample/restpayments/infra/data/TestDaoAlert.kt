package sample.restpayments.infra.data


import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import sample.restpayments.domain.*

@RunWith(SpringRunner::class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
@SpringBootTest
class TestDaoAlert {

    @Autowired
    lateinit var alertRepository: AlertRepository

    @Test
    fun testAlertRepo() {

        assertEquals(4, alertRepository.list().count())
        val alert = alertRepository.save(Alert(AlertType.BIG_PAYMENT, "woohoo", AlertId("SOME_ID"), PaymentId("a-payment-id")))
        assertEquals(0, alert.events.count())
        assertEquals(5, alertRepository.list().count())

        val alertById = alertRepository.findById(alert.alertId).get()

        assertEquals(alertById, alert)


    }
}