package sample.restpayments.infra.rest

import org.hamcrest.Matchers.containsString
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.hateoas.MediaTypes
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.header
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import sample.restpayments.domain.*
import java.math.BigDecimal
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "build/generated-snippets")
class PaymentControllerTest {


    @Autowired
    lateinit var mockMvc: MockMvc

    @MockBean
    lateinit var paymentService: PaymentService

    @MockBean
    lateinit var paymentRepository: PaymentRepository

    @Test
    fun postPayment() {

        val payment = Payment(PaymentId("adsf"), BigDecimal.TEN)
        given(paymentService.createPayment(NewPayment("10"))).willReturn(payment)

        this.mockMvc
                .perform(
                        post("/payments")
                                .accept(MediaTypes.HAL_JSON)
                                .contentType(MediaTypes.HAL_JSON_UTF8)
                                .content("{\"amount\":\"10\"}")
                )
                .andDo(print())
                .andExpect(status().isCreated)
                .andExpect(header().string("Location", containsString(payment.paymentId.id)))
                .andDo(document("payments"))
    }

    @Test
    fun cancelPayment() {

        val payment = Payment(PaymentId("123"), BigDecimal.TEN)
        given(paymentRepository.findById(PaymentId("123"))).willReturn(Optional.of(payment))


        this.mockMvc
                .perform(
                        post("/payments/123/cancellations")
                                .accept(MediaTypes.HAL_JSON)
                                .contentType(MediaTypes.HAL_JSON_UTF8)
                                .content("""{"reason": "Message tooooo loooooooong" }""")
                )
                .andDo(print())
                .andExpect(status().isBadRequest)
                .andDo(document("payments/cancellation/error"))
    }

}