package sample.restpayments.infra.rest

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.hateoas.MediaTypes
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import sample.restpayments.domain.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "build/generated-snippets")
class AlertsRepositoryTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var alertsRepository: AlertRepository

    @Test
    fun getAlert() {
        val alertId = AlertId("id")
        alertsRepository.save((Alert(type = AlertType.BIG_PAYMENT, alertId = alertId, paymentId = PaymentId("a-payment-id"))))

        this.mockMvc
                .perform(
                        get(REQUEST_MAPPING, alertId.id)
                                .accept(MediaTypes.HAL_JSON)
                                .contentType(MediaTypes.HAL_JSON_UTF8)
                )
                .andDo(print())
                .andExpect(status().isOk)
                .andExpect(content().contentTypeCompatibleWith(MediaTypes.HAL_JSON))
                .andDo(document("alerts/get"))
    }

}