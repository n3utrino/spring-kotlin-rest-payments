package sample.restpayments.infra.rest

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.hateoas.MediaTypes
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.operation.preprocess.Preprocessors.*
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import sample.restpayments.domain.*
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "build/generated-snippets")
class AlertsControllerTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    @MockBean
    lateinit var alertRepository: AlertRepository

    @Test
    fun postAssignment() {

        val docs = document(
                "alerts/post-assignment",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint())
        )

        val alertId = AlertId("an-alert-id")

        given(alertRepository.findById(alertId))
                .willReturn(Optional.of(Alert(type = AlertType.BIG_PAYMENT, alertId = alertId, paymentId = PaymentId("a-payment-id"))))

        this.mockMvc
                .perform(
                        post("$REQUEST_MAPPING/{id}/assignment", 1)
                                .accept(MediaTypes.HAL_JSON)
                                .contentType(MediaTypes.HAL_JSON_UTF8)
                                .content("{\"employee\":\"ich\"}")
                )
                .andDo(print())
                .andExpect(status().isCreated)
                //.andExpect(content().contentTypeCompatibleWith(MediaTypes.HAL_JSON))
                .andDo(docs)
    }

}