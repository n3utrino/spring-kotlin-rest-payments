package sample.restpayments.infra

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.hateoas.UriTemplate
import org.springframework.hateoas.hal.CurieProvider
import org.springframework.hateoas.hal.DefaultCurieProvider


const val CURIE_NAMESPACE = "restpay"

@Configuration
class Config {
    @Bean("curieProvider")
    fun curieProvider(): CurieProvider {
        return DefaultCurieProvider(CURIE_NAMESPACE, UriTemplate("/docs/api-doc.html#{rel}"))
    }
}