package sample.restpayments.infra.data

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import sample.restpayments.domain.*
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table


/**
 * Exposes IPaymentData as REST Ressource
 */
@RepositoryRestResource(path = "/payments", collectionResourceRel = "payments", itemResourceRel = "payment")
internal interface SpringDataPaymentRepository : PagingAndSortingRepository<PaymentDbo, String> {
    fun findByStatusAndId(paymentStatus: PaymentStatus, id: String): Optional<PaymentDbo>
}


@Repository
internal class JpaPaymentRepositoryImpl(val springDataPaymentRepository: SpringDataPaymentRepository) : PaymentRepository {
    override fun save(entity: Payment): Payment {
        return springDataPaymentRepository.save(PaymentDbo(entity)).toEntity()
    }

    override fun findById(id: PaymentId): Optional<Payment> {
        return springDataPaymentRepository.findById(id.id).map { it.toEntity() }
    }

    override fun list(): Iterable<Payment> {
        return springDataPaymentRepository.findAll().map { it.toEntity() }
    }

}

@Entity
@Table(name = "T_Payments")
internal class PaymentDbo(alert: Payment) : EntityDbo<Payment, IPaymentData>(alert), IPaymentData {

    @Id
    val id = domainObject.paymentId.id
    override val amount = domainObject.amount
    override var cancelReason = domainObject.cancelReason
    override var canceledBy = domainObject.canceledBy
    override var status = domainObject.status
    override var message = domainObject.message


    override fun toEntity() = Payment(PaymentId(id), amount, status, canceledBy)
}