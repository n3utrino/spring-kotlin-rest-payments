package sample.restpayments.infra.data

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.stereotype.Repository
import sample.restpayments.domain.*
import java.util.*
import javax.persistence.*


@Repository
internal class JpaAlertRepositoryImpl(val springDataAlertRepositoryDelegate: SpringDataAlertRepositoryDelegate) : AlertRepository {
    override fun save(entity: Alert): Alert {
        return springDataAlertRepositoryDelegate.save(AlertDbo(entity)).toEntity()
    }

    override fun findById(id: AlertId): Optional<Alert> {
        return springDataAlertRepositoryDelegate.findById(id.id).map { it.toEntity() }
    }

    override fun list(): Iterable<Alert> {
        return springDataAlertRepositoryDelegate.findAll().map { it.toEntity() }
    }

}

@Entity
@Table(name = "T_Alert_Events")
class AlertEventDbo(alertEvent: AlertEvent, alertId: AlertId) : EntityDbo<AlertEvent, IAlertEventData>(alertEvent), IAlertEventData {

    @Id
    val id = alertId.id + domainObject.timestamp
    override val message = domainObject.message
    override var user = domainObject.user
    override var timestamp = domainObject.timestamp

    override fun toEntity() = AlertEvent(user, message, timestamp)

}


@Entity
@Table(name = "T_Alerts")
class AlertDbo(alert: Alert) : EntityDbo<Alert, IAlertData>(alert), IAlertData {

    @Id
    val id = domainObject.alertId.id
    override val type = domainObject.type
    override val message = domainObject.message
    override var locked = domainObject.locked
    override var status = domainObject.status
    @JsonIgnore
    val paymentId = domainObject.paymentId.id

    @OneToMany(fetch = FetchType.LAZY, cascade = [(CascadeType.ALL)])
    override val events = domainObject.events.map { AlertEventDbo(it as AlertEvent, domainObject.alertId) }

    override fun toEntity() = Alert(type, message, AlertId(id), events.map { it.toEntity() }, PaymentId(paymentId))
}