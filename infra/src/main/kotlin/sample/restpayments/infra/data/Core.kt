package sample.restpayments.infra.data

import javax.persistence.MappedSuperclass
import javax.persistence.Transient
import javax.persistence.Version

@MappedSuperclass
abstract class EntityDbo<T, I>(@Transient protected val domainObject: T) {
    abstract fun toEntity(): I

    @Version
    val version: Long = 0
}
