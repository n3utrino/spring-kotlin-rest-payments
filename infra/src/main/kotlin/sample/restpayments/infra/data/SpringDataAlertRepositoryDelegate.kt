package sample.restpayments.infra.data

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(path = "/alerts", collectionResourceRel = "alerts", itemResourceRel = "alert")
internal interface SpringDataAlertRepositoryDelegate : PagingAndSortingRepository<AlertDbo, String>