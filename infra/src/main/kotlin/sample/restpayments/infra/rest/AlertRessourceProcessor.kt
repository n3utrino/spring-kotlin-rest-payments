package sample.restpayments.infra.rest

import org.springframework.hateoas.Link
import org.springframework.hateoas.Resource
import org.springframework.hateoas.ResourceProcessor
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn
import org.springframework.stereotype.Component
import sample.restpayments.domain.PaymentId
import sample.restpayments.infra.CURIE_NAMESPACE
import sample.restpayments.infra.data.AlertDbo

@Component
class AlertResourceProcessor(val alertLinks: AlertLinks, val paymentLinks: PaymentLinks) : ResourceProcessor<Resource<AlertDbo>> {
    override fun process(resource: Resource<AlertDbo>?): Resource<AlertDbo> {
        val alert = resource!!.content

        resource.add(paymentLinks.getSelfLink(PaymentId(alert.paymentId)))

        if (alert.locked) {
            resource.add(alertLinks.getUnLockLink(alert))

        } else {
            resource.add(alertLinks.getClaimLink(alert))
            resource.add(alertLinks.getLockLink(alert))
        }

        return resource
    }
}

const val ASSIGNMENT_LINK = "$CURIE_NAMESPACE:assignment"
const val LOCK_LINK = "$CURIE_NAMESPACE:lock"
const val UNLOCK_LINK = "$CURIE_NAMESPACE:unlock"

@Component
class AlertLinks {


    fun getClaimLink(alert: AlertDbo): Link {
        return linkTo(methodOn(AlertsController::class.java).getAssignment(alert.id)).withRel(ASSIGNMENT_LINK)
    }

    fun getLockLink(alert: AlertDbo): Link {
        return linkTo(methodOn(AlertsController::class.java).lockAlert(alert.id)).withRel(LOCK_LINK)
    }

    fun getUnLockLink(alert: AlertDbo): Link {
        return linkTo(methodOn(AlertsController::class.java).unlockAlert(alert.id)).withRel(UNLOCK_LINK)
    }

}