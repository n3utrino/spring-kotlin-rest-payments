package sample.restpayments.infra.rest

import org.springframework.data.rest.webmvc.RepositoryRestController
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import sample.restpayments.domain.NewPayment
import sample.restpayments.domain.PaymentRepository
import sample.restpayments.domain.PaymentService
import java.net.URI


@CrossOrigin
@RepositoryRestController
@Transactional
class PaymentController(val paymentLinks: PaymentLinks, val paymentService: PaymentService, val paymentRepository: PaymentRepository, val paymentResourceAssembler: PaymentResourceAssembler) {

    @PostMapping("/payments")
    fun createPayment(@RequestBody newPayment: NewPayment): ResponseEntity<Any> {
        val createdPayment = paymentService.createPayment(newPayment)

        return ResponseEntity.created(URI.create(paymentLinks.getSelfLink(createdPayment).href)).build()

    }

}