package sample.restpayments.infra.rest

import org.springframework.hateoas.Resource
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import sample.restpayments.domain.*
import sample.restpayments.infra.data.SpringDataPaymentRepository
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/payments/{id}/cancellations")
@Transactional
internal class PaymentCancellationController(val paymentService: PaymentService, val springDataPaymentRepository: SpringDataPaymentRepository) {

    @PostMapping()
    fun cancel(@PathVariable id: String, @RequestBody @Valid cancellation: PaymentCancellation): ResponseEntity<Resource<Payment>> {
        paymentService.cancelPayment(PaymentId(id), cancellation)

        return ResponseEntity(HttpStatus.ACCEPTED)
    }

    @GetMapping()
    fun getCancellation(@PathVariable id: String): ResponseEntity<Resource<PaymentCancellation>> {
        val payment = springDataPaymentRepository.findByStatusAndId(PaymentStatus.CANCELED, id).get()
        return ResponseEntity(Resource(PaymentCancellation("cancelled by ${payment.canceledBy}")), HttpStatus.OK)
    }


}

