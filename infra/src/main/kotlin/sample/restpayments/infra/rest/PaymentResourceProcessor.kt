package sample.restpayments.infra.rest

import org.springframework.hateoas.*
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn
import org.springframework.hateoas.mvc.ResourceAssemblerSupport
import org.springframework.stereotype.Component
import sample.restpayments.domain.Payment
import sample.restpayments.domain.PaymentCancellation
import sample.restpayments.domain.PaymentId
import sample.restpayments.infra.data.PaymentDbo


@Component
internal class PaymentResourceProcessor(val paymentLinks: PaymentLinks) : ResourceProcessor<Resource<PaymentDbo>> {
    override fun process(resource: Resource<PaymentDbo>?): Resource<PaymentDbo>? {
        val payment = resource!!.content.toEntity()
        if (payment.cancelable()) {
            resource.add(paymentLinks.getCancelLink(payment))
        }
        return resource
    }
}

@Component
class PaymentLinks(val entityLinks: EntityLinks) {
    fun getCancelLink(payment: Payment): Link {
        return linkTo(methodOn(PaymentCancellationController::class.java)
                .cancel(payment.paymentId.id, PaymentCancellation("")))
                .withRel("cancel")
    }

    fun getSelfLink(payment: Payment): Link {
        return getSelfLink(payment.paymentId)
    }

    fun getSelfLink(paymentId: PaymentId): Link {
        return entityLinks.linkToSingleResource(PaymentDbo::class.javaObjectType, paymentId.id)
    }

}


class PaymentResource(val payment: Payment) : ResourceSupport()

@Component
class PaymentResourceAssembler(val paymentLinks: PaymentLinks) : ResourceAssemblerSupport<Payment, PaymentResource>(Payment::class.java, PaymentResource::class.java) {
    override fun toResource(entity: Payment?): PaymentResource {
        val resource = PaymentResource(entity!!)
        resource.add(paymentLinks.getSelfLink(entity))
        if (entity.cancelable()) {
            resource.add(paymentLinks.getCancelLink(entity))
        }
        return resource
    }
}