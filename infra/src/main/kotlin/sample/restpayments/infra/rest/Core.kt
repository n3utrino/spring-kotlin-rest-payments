package sample.restpayments.infra.rest

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import sample.restpayments.domain.BusinessException

@ControllerAdvice
class BusinessServiceExceptionHandler {

    private data class RestException(val message: String)

    @ExceptionHandler(BusinessException::class)
    fun handleBusinessException(businessException: BusinessException): ResponseEntity<Any> {
        return ResponseEntity(RestException(businessException.message!!), HttpStatus.BAD_REQUEST)
    }
}