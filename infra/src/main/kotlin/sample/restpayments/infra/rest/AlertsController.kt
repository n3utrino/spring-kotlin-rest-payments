package sample.restpayments.infra.rest

import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.ResourceSupport
import org.springframework.hateoas.Resources
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import sample.restpayments.domain.*
import sample.restpayments.domain.AlertAssignment

const val REQUEST_MAPPING = "/alerts/"

@Controller
@RequestMapping(REQUEST_MAPPING, produces = [MediaTypes.HAL_JSON_VALUE])
@Transactional
class AlertsController(val alertRepository: AlertRepository) {

    @PostMapping("{id}/assignment")
    fun assingAlert(@PathVariable("id") id: String, @RequestBody alertAssignment: AlertAssignment): ResponseEntity<Any> {
        alertRepository.findById(AlertId(id)).ifPresent { it -> it.assignTo(alertAssignment) }
        return ResponseEntity(HttpStatus.CREATED)
    }

    @GetMapping("{id}/assignment")
    fun getAssignment(@PathVariable("id") id: String): ResponseEntity<AlertAssignment> {
        val alert = alertRepository.findById(AlertId(id)).get()
        return ResponseEntity(alert.assignment, HttpStatus.OK)
    }

    @PostMapping("{id}/locks")
    fun lockAlert(@PathVariable("id") id: String): ResponseEntity<Any> {
        alertRepository.findById(AlertId(id)).get().lock()
        return ResponseEntity(HttpStatus.CREATED)
    }

    @DeleteMapping("{id}/locks")
    fun unlockAlert(@PathVariable("id") id: String): ResponseEntity<Any> {
        alertRepository.findById(AlertId(id)).get().unlock()
        return ResponseEntity(HttpStatus.OK)
    }

    @GetMapping("{id}/events")
    fun getAlertEvents(@PathVariable("id") alert: Alert): ResponseEntity<Resources<IAlertEventData>> {
        return ResponseEntity(Resources(alert.events), HttpStatus.OK)
    }
}

class AlertAssignment constructor(val employee: String) : ResourceSupport()
