package sample.restpayments.domain

import java.math.BigDecimal
import java.util.*
import javax.validation.Valid
import javax.validation.constraints.Size

interface PaymentRepository : AbstractRepository<Payment, PaymentId>

interface IPaymentData {
    val amount: BigDecimal
    var status: PaymentStatus
    var canceledBy: String
    var cancelReason: String
    var message: String
}

data class Payment(
        val paymentId: PaymentId,
        override val amount: BigDecimal = BigDecimal("0.0"),
        override var status: PaymentStatus = PaymentStatus.PENDING,
        override var canceledBy: String = "",
        @get:Size(max = 200)
        override var cancelReason: String = "",
        override var message: String = ""
) : AbstractAggregate(), IPaymentData {

    fun start(): Payment {
        if (amount.compareTo(BigDecimal("1000000000")) == 1) {
            throw PaymentTooBigException()
        }
        return this
    }

    fun cancelable(): Boolean {
        return status.equals(PaymentStatus.PENDING)
    }

    fun cancel(paymentCancellation: PaymentCancellation) {
        canceledBy = "USER"
        status = PaymentStatus.CANCELED
        cancelReason = paymentCancellation.reason
    }


}

enum class PaymentStatus {
    PENDING, PAID, CANCELED
}

data class BigPaymentEvent(val paymentId: PaymentId)
data class NewPayment(var amount: String, var message: String = "")

data class PaymentId(val id: String)
data class PaymentCancellation(@get:Size(max = 10) val reason: String)

interface PaymentService {
    fun createPayment(newPayment: NewPayment): Payment
    fun cancelPayment(id: PaymentId, @Valid cancellation: PaymentCancellation)
}


class PaymentServiceImpl(private val paymentRepository: PaymentRepository, private val alertRepository: AlertRepository) : PaymentService {

    override fun createPayment(newPayment: NewPayment): Payment {
        val payment = Payment(
                paymentId = PaymentId(UUID.randomUUID().toString()),
                amount = BigDecimal(newPayment.amount)
        )

        payment.start()
        alertRepository.save(
                Alert(AlertType.BIG_PAYMENT,
                        "Some Payment Did Happen",
                        AlertId(UUID.randomUUID().toString()),
                        payment.paymentId))

        return paymentRepository.save(payment)
    }

    override fun cancelPayment(id: PaymentId, cancellation: PaymentCancellation) {
        val payment = paymentRepository.findById(id).get()
        payment.cancel(cancellation)
        paymentRepository.save(payment)

    }


}

class PaymentTooBigException : BusinessException("This Payment is too big") {}