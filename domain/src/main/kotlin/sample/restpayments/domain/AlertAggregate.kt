package sample.restpayments.domain

import java.time.LocalDateTime


interface AlertRepository : AbstractRepository<Alert, AlertId>

class AlertAssignment constructor(val employee: String)

enum class AlertType {
    BIG_PAYMENT
}

enum class AlertStatus {
    OPEN, RESOLVED
}

data class AlertId(val id: String)

interface IAlertEventData {
    val user: String
    val message: String
    val timestamp: LocalDateTime
}

data class AlertEvent(override val user: String, override val message: String, override val timestamp: LocalDateTime = LocalDateTime.now()) : IAlertEventData

interface IAlertData {
    val type: AlertType
    val message: String
    //var assignment: AlertAssignment?
    var locked: Boolean
    var status: AlertStatus
    val events: List<IAlertEventData>
}

data class Alert(
        override val type: AlertType,
        override val message: String = "",
        val alertId: AlertId,
        val paymentId: PaymentId) : IAlertData {

    constructor(type: AlertType,
                message: String = "",
                alertId: AlertId, events: Collection<AlertEvent>, paymentId: PaymentId) : this(type, message, alertId, paymentId) {
        this.events.addAll(events)
    }

    var assignment: AlertAssignment? = null
    override var locked: Boolean = false
    override var status: AlertStatus = AlertStatus.OPEN

    fun assignTo(alertAssignment: AlertAssignment) {
        assignment = alertAssignment
        addEvent(AlertEvent("system", "Assignment changed to " + alertAssignment.employee))
    }

    fun lock() {
        locked = true
        addEvent(AlertEvent("system", "Alert Locked"))
    }

    fun unlock() {
        locked = false
        addEvent(AlertEvent("system", "Alert Un Locked"))
    }


    override val events = mutableListOf<IAlertEventData>()

    private fun addEvent(newItem: AlertEvent) = this.events.plusAssign(newItem)

}