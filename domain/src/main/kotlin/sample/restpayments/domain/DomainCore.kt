package sample.restpayments.domain

import java.util.*

abstract class AbstractAggregate {
    private val events = mutableListOf<Any>()

    fun registerEvent(event: Any) {
        events.add(event)
    }

}

interface AbstractRepository<T, I> {
    fun save(entity: T): T
    fun findById(id: I): Optional<T>
    fun list(): Iterable<T>
}

abstract class BusinessException(message: String) : RuntimeException(message)