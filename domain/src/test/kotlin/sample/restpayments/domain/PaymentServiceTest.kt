package sample.restpayments.domain

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import java.util.*
import javax.validation.Validation


class TestPaymentRepositoryImpl : PaymentRepository {

    val store: MutableMap<PaymentId, Payment> = mutableMapOf()

    override fun save(entity: Payment): Payment {
        store.put(entity.paymentId, entity)
        return entity
    }

    override fun findById(id: PaymentId): Optional<Payment> {
        return Optional.of(store.getValue(id))
    }

    override fun list(): Iterable<Payment> {
        return store.values
    }

}

class AlertRepoTestImpl : AlertRepository {

    val store: MutableMap<AlertId, Alert> = mutableMapOf()

    override fun save(entity: Alert): Alert {
        store.put(entity.alertId, entity)
        return entity
    }

    override fun findById(id: AlertId): Optional<Alert> {
        return Optional.of(store.getValue(id))
    }

    override fun list(): Iterable<Alert> {
        return store.values
    }

}


class PaymentServiceTest {

    private val paymentRepository: PaymentRepository = TestPaymentRepositoryImpl()
    private val alertRepository: AlertRepository = AlertRepoTestImpl()
    private val paymentServiceImpl = PaymentServiceImpl(paymentRepository, alertRepository)

    @Test
    fun testNewPayment() {

        assertThat(paymentServiceImpl).isNotNull
        paymentServiceImpl.createPayment(NewPayment("12"))

        assertThat(paymentRepository.list().count()).isEqualTo(1)
        assertThat(alertRepository.list().count()).isEqualTo(1)


    }

    @Test
    fun paymentTooBigException() {

        assertThat(paymentServiceImpl).isNotNull
        assertThatThrownBy { paymentServiceImpl.createPayment(NewPayment("1000000001")) }
                .isInstanceOf(PaymentTooBigException::class.java)

        assertThat(paymentRepository.list().count()).isEqualTo(0)
        assertThat(alertRepository.list().count()).isEqualTo(0)


    }

    @Test
    fun cancelPayment() {
        val reason = "Eifach So"

        val id = paymentServiceImpl.createPayment(NewPayment("9")).paymentId

        assertThat(paymentRepository.list().count()).isEqualTo(1)
        assertThat(alertRepository.list().count()).isEqualTo(1)

        paymentServiceImpl.cancelPayment(id, PaymentCancellation(reason))

        assertThat(paymentRepository.findById(id).get().cancelReason).isEqualTo(reason)

    }

    @Test
    fun cancelBeanValidation() {

        val factory = Validation.buildDefaultValidatorFactory()
        val validator = factory.validator
        assertThat(validator.validate(PaymentCancellation("1"))).isEmpty()
        assertThat(validator.validate(PaymentCancellation("1231231231321221312213211123231231"))).isNotEmpty

    }
}