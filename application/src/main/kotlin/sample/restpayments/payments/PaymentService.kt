package sample.restpayments.payments

import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import sample.restpayments.domain.AlertRepository
import sample.restpayments.domain.PaymentRepository
import sample.restpayments.domain.PaymentService
import sample.restpayments.domain.PaymentServiceImpl

@Component
class PaymentServiceBean {
    @Bean
    fun paymentService(paymentRepository: PaymentRepository, alertRepository: AlertRepository): PaymentService {
        return PaymentServiceImpl(paymentRepository, alertRepository)
    }
}