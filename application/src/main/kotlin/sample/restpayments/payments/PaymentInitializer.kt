package sample.restpayments.payments

import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import sample.restpayments.domain.NewPayment
import sample.restpayments.domain.PaymentService

@Service
class PaymentInitializer(val paymentService: PaymentService) {

    @EventListener
    fun init(applicationReadyEvent: ApplicationReadyEvent) {
        paymentService.createPayment(NewPayment(amount = "30.0"))
        paymentService.createPayment(NewPayment(amount = "50.0"))
        paymentService.createPayment(NewPayment(amount = "90.0"))
        paymentService.createPayment(NewPayment(amount = "300000.0"))
    }
}