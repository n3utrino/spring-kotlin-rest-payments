= Rest Payments Service

Service with Onion Architecture
https://www.infoq.com/news/2014/10/ddd-onion-architecture


=== domain
Entities, Repositories, Serivces __NO DEPENDENCIES__
to any JPA, Spring, Rest or other technical frameworks.

* Testable without database mocking
* Domain knowledge

=== infra
JPA, REST implementations of Domain Repos

* Database acces repos
* Rest Ressources

=== application
Runnable Spring Boot application and configuration

== Todo
* [ ] Domain Events
* [ ] Transactions with transaction interceptor https://github.com/cstettler/ddd-to-the-code-workshop-sample/blob/master/ddd-to-the-code-workshop-support/src/main/java/com/github/cstettler/dddttc/support/infrastructure/persistence/TransactionConfiguration.java
* [ ] Authorized Projections
* [ ] Logging Technical and Business logging
* [ ] Add non Repo entities to HAL profile
* [ ] Typescript Types from Json schema
* [x] REST Resource Entity version
* [x] Validation
* [x] Kotlin
* [x] Exception Handling
* [x] Spring Boot
* [x] HAL
* [x] HAL Browser
* [x] Gradle
* [x] Unittests
* [x] Documentation
* [ ] [.line-through]##Consumer Driven Contract Testing##


== References
* https://docs.spring.io/spring-data/rest/docs/3.1.2.RELEASE/reference/html/
* https://spring.io/guides/gs/testing-restdocs/


== Issues

* build does not work with java 11
