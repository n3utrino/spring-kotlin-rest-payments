import {TestBed} from '@angular/core/testing';

import {ExternalConfigurationService} from './external-configuration.service';

describe('ExternalConfigurationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExternalConfigurationService = TestBed.get(ExternalConfigurationService);
    expect(service).toBeTruthy();
  });
});
