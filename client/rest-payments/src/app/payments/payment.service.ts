import {Injectable, Injector} from '@angular/core';
import {RestService} from "angular4-hal";
import {Payment} from "./payment.model";

@Injectable({
  providedIn: 'root'
})
export class PaymentService extends RestService<Payment> {

  constructor(injector: Injector) {
    super(Payment, 'payments', injector);
  }
}
