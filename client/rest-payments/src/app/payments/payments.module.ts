import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PaymentListComponent} from "./payment-list/payment-list.component";
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';
import {PaymentActionsComponent} from './payment-actions/payment-actions.component';

@NgModule({
  declarations: [PaymentListComponent, PaymentActionsComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatButtonModule
  ],
  exports: [PaymentListComponent]
})
export class PaymentsModule {
}
