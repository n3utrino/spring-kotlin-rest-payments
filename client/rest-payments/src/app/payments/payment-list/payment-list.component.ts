import {Component, OnInit} from '@angular/core';
import {PaymentService} from "../payment.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";


@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.sass']
})
export class PaymentListComponent implements OnInit {

  displayedColumns = ["amount", "status", "actions"];
  payments: any[] = [];

  constructor(private paymentService: PaymentService, private http: HttpClient) {

  }

  ngOnInit() {
    this.getAllPayments();
  }

  getAllPayments() {
    this.paymentService.getAll().subscribe((payments) => {
      this.payments = payments;
    })
  }

  performAction(action: { name: string, link: { href: string } }) {
    console.log(action);

    switch (action.name) {
      case "restpay:cancel" :
        this.cancelPayment(action)

    }

  }

  cancelPayment(action: { name: string, link: { href: string } }) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.http.post(action.link.href, {"reason": "some reason"}, httpOptions)
      .subscribe(() => {
        console.log("Done")
      })
  }

}
