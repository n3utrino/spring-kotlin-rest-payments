import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-payment-actions',
  templateUrl: './payment-actions.component.html',
  styleUrls: ['./payment-actions.component.sass']
})
export class PaymentActionsComponent implements OnInit {

  constructor() {
  }

  @Input("hal-links") halLinks: any;
  @Output() paymentAction = new EventEmitter<String>();

  possibleActions: any[] = [];

  ngOnInit() {
    for (let halLinksKey in this.halLinks) {
      if (halLinksKey.startsWith("restpay:")) {
        this.possibleActions.push({name: halLinksKey, link: this.halLinks[halLinksKey]})
      }
    }


  }

  doAction(action: string) {
    this.paymentAction.emit(action);
  }

}
