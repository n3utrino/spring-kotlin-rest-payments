import {Resource} from "angular4-hal";

export class Payment extends Resource {
  amount: string;
  status: string;
}
