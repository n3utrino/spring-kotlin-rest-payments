import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AngularHalModule} from 'angular4-hal';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PaymentsModule} from "./payments/payments.module";
import {ExternalConfigurationService} from "./external-configuration.service";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PaymentsModule, HttpClientModule,
    BrowserAnimationsModule,
    AngularHalModule.forRoot()
  ],
  providers: [
    //ResourceService,
    {provide: 'ExternalConfigurationService', useClass: ExternalConfigurationService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
