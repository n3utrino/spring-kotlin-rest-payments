import {Injectable} from '@angular/core';
import {ExternalConfiguration, ExternalConfigurationHandlerInterface, ResourceHelper} from "angular4-hal";
import {HttpClient, HttpHeaders} from "@angular/common/http";

// noinspection JSAnnotator
@Injectable({
  providedIn: 'root'
})
export class ExternalConfigurationService implements ExternalConfigurationHandlerInterface {

  constructor(private http: HttpClient) {
    ResourceHelper.headers = new HttpHeaders();
  }

  getProxyUri(): string {
    return 'http://localhost:8080/';
  }

  getRootUri(): string {
    return 'http://localhost:4200/';
  }

  getHttp(): HttpClient {
    return this.http;
  }


  getExternalConfiguration(): ExternalConfiguration {
    return null;
  }

  setExternalConfiguration(externalConfiguration: ExternalConfiguration) {
  }

  deserialize(): any {
  }

  serialize(): any {
  }
}
