import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PaymentListComponent} from "./payments/payment-list/payment-list.component";

const routes: Routes = [
  {path: "", component: PaymentListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
